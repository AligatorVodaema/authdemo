#FastAPI server
from fastapi.param_functions import Header
from loguru import logger
from typing import Optional
import re
import base64
import hmac
import hashlib
import json

from fastapi import FastAPI, Form, Cookie, Body, Query
from fastapi.responses import Response
from typing import Dict, Any

logger.add('info.log', format="{time} {level} {message}")

app = FastAPI()

SECRET_KEY = '9db4de3b3916c9b090dcb98d5d8977abcd07bd1c03617e12dac1e6078da5fba7'
PASSWORD_SALT = 'c3ce7bd9f14e434cced871d6a232e2737bf17d216fb6650766d275a1a0d2848d'

users = {
    'user@user.com': {
        'name': 'Вася',
        'password': '3e8c5607d3b4dc04e402f34385a4555735a5a054c4e03fbdad5608fe8ea3dbc9',
        'balance': 100_000

    },
    'petr@user.com': {
        'name': 'Пётр',
        'password': '2632e02849d0e5796b48097b9726f28ea65ddf3a846c34380e0e7c28646d8b10',
        'balance': 555_555
    }
}
def verify_password(username: str, password: str) -> bool:
    password_hash = hashlib.sha256((password + PASSWORD_SALT).encode()).hexdigest().lower()
    stored_password_hash = users[username]['password'].lower()
    return password_hash == stored_password_hash

def sign_data(data: str) -> str:
    '''Возвращает подписанные данные data'''
    return hmac.new(
        SECRET_KEY.encode(),
        msg=data.encode(),
        digestmod=hashlib.sha256
    ).hexdigest().upper()

def get_username_from_signed_string(username_signed: str) -> Optional[str]:
    username_base64, sign = username_signed.split('.')
    username = base64.b64decode(username_base64.encode()).decode()
    valid_sign = sign_data(username)
    if hmac.compare_digest(valid_sign, sign):
        return username

@app.get('/')
def index_page(username: Optional[str] = Cookie(default=None)):
    with open('templates/login.html', 'r') as f:
        login_page = f.read()
    if not username:
        return Response(login_page, media_type='text/html')
    valid_username = get_username_from_signed_string(username)
    if not valid_username:
        response = Response(login_page, media_type='text/html')
        response.delete_cookie(key='username')
        return response
    try:
        user = users[valid_username]
    except KeyError:
        response = Response(login_page, media_type='text/html')
        response.delete_cookie(key='username')
        return response
    return Response(
        f'И снова здравствуйте, {users[valid_username]["name"]}!<br />'
        f'Баланс {users[valid_username]["balance"]}', media_type='text/html')
    


@app.post('/login')
def process_login_page(data: dict = Body(...)):
    print('login data is', data)
    username = data['username']
    password = data['password']
    user = users.get(username)
    if not user or not verify_password(username, password):
        return Response(
            json.dumps({
                'success': False,
                'message': 'Ты кто такой вообще?'
            }),
            media_type='application/json')
    
    response =  Response(
        json.dumps({
            'success': True,
            'message': f'Привет, {user["name"]}<br /> Баланс: {user["balance"]}'
        }),
        media_type='application/json')
    
    username_signed = base64.b64encode(username.encode()).decode() + '.' + \
        sign_data(username)

    response.set_cookie(key='username', value=username_signed)
    return response

@app.post('/unify_phone_from_json')
def unify_phone_from_json(data: dict = Body(...)):
    logger.info(f'phone_json_data is {data!r}')
    phone = data["phone"]
    result = parse_phone(phone)
    logger.info(f'result phone is {result!r}')
    return Response(result, media_type='text/html')


@app.post('/unify_phone_from_form')
def unify_phone_from_form(phone: str = Form(...)):
    logger.info(f'result FORM, phone is {phone!r}')
    result = parse_phone(phone)
    return Response(result, media_type='text/html')

@app.get('/unify_phone_from_query')
def unify_phone_from_query(phone: str = Query(...)):
    logger.info(f'result QUERY, phone is {phone!r}')
    result = parse_phone(phone)
    return Response(result, media_type='text/html')

@app.get('/unify_phone_from_cookies')
def unify_phone_from_cookie(phone: Optional[str] = Cookie(None)):
    logger.info(f'got cookie from COOKIE func {phone!r}')
    if phone:
        result = parse_phone(phone)
        return Response(result, media_type='text/html')


def parse_phone(phone: str) -> str:
    ex = re.findall(re.compile(r'[\d]'), phone)
    if len(ex) == 10 or len(ex) == 11:

        if ''.join(ex).startswith(('7', '8')):
            ex.pop(0)
            result = '8 ' + '(' + ''.join(ex[:3]) + ') ' + ''.join(ex[3:6]) + '-' + ''.join(ex[6:8]) + '-' + ''.join(ex[8:10])

        elif ''.join(ex).startswith('9'):
            result = '8 ' + '(' + ''.join(ex[:3]) + ') ' + ''.join(ex[3:6]) + '-' + ''.join(ex[6:8]) + '-' + ''.join(ex[8:10])
        else:
            result = ''.join(ex)
    else:
        result = ''.join(ex)
    return result